﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Yugamiru
{

    public class SettingDataMgr
    {
        int m_iCount;
        SettingDataElement[] m_pElement;

        // Ý’èƒ}ƒXƒ^[ƒf[ƒ^. 
        public SettingDataMgr()
        {
            m_iCount = 0;
            m_pElement = new SettingDataElement[m_iCount];

        }




        public bool ReadFromFile(string lpszFolderName, string lpszFileName)
        {
            char[] buf = new char[1024];

            List<SettingDataElement> listElement = new List<SettingDataElement>();
            if (m_pElement != null)
            {
                //delete[] m_pElement;
                m_pElement = null;
            }
            m_iCount = 0;

            DualSourceTextReader DualSourceTextReader = new DualSourceTextReader();
            int i = DualSourceTextReader.Open(lpszFolderName, lpszFileName);
            if (i == 0)
            {
                return false;
            }
            while (!(DualSourceTextReader.IsEOF()))
            {
                buf[0] = '\0';
                DualSourceTextReader.Read(ref buf, 1024);
                //var t = File.ReadAllLines(@"Resources\ResultAction.inf");
                SettingDataElement Element = new SettingDataElement();
                if (Element.ReadFromString(buf))
                {
                    listElement.Add(Element);
                }
            }

            // €–Ú”•ª‚¾‚¯ƒƒ‚ƒŠŠm•Û.
            int iSize = listElement.Count;
            if (iSize <= 0)
            {
                return true;
            }
            m_pElement = new SettingDataElement[iSize];
            if (m_pElement == null)
            {
                return false;
            }
            m_iCount = iSize;

            for (int j = 0; j < listElement.Count; j++)
            {
                m_pElement[j] = listElement[j];
            }

            return true;
        }



        public int GetElementCount()
        {
            return m_iCount;
        }

        public SettingDataElement GetElement(int iIndex)
        {
            if ((iIndex < 0) || (m_iCount <= iIndex))
            {
                return null;
            }
            return (m_pElement[iIndex]);
        }

        public bool GetValriableValue(string lpszVariableName, ref string strValue)
        {
            int i = 0;
            for (i = 0; i < m_iCount; i++)
            {
                if (string.Compare(m_pElement[i].m_strVariableName, lpszVariableName) == 0)
                {
                    strValue = m_pElement[i].m_strValue;
                    return true;
                }
            }
            return false;
        }

        public bool AddVariableValue(string lpszVariableName, string strValue)
        {
            // ‚Ü‚¸A‚·‚Å‚É•Ï”‚ª“o˜^Ï‚©‚Ç‚¤‚©‚ðƒ`ƒFƒbƒN‚·‚é.
            int i = 0;
            for (i = 0; i < m_iCount; i++)
            {
                if (string.Compare(m_pElement[i].m_strVariableName, lpszVariableName) == 0)
                {
                    m_pElement[i].m_strValue = strValue;
                    return true;
                }
            }
            // “o˜^Ï‚Å‚Í‚È‚¢‚Ì‚ÅAV‚½‚É“o˜^‚·‚é.
            int iNewCount = m_iCount + 1;

            SettingDataElement[] pElement = new SettingDataElement[iNewCount];
            if (pElement == null)
            {
                return false;
            }
            for (i = 0; i <= m_iCount; i++)
            {
                pElement[i] = m_pElement[i];
            }
            pElement[m_iCount].m_strVariableName = lpszVariableName;
            pElement[m_iCount].m_strValue = strValue;

            if (m_pElement != null)
            {

                m_pElement = null;
            }
            m_iCount = iNewCount;
            m_pElement = pElement;

            return true;
        }
        public bool WriteToFile(string lpszFolderName, string lpszFileName)
        {

            string strFilePath = lpszFolderName + "\\";
            strFilePath += lpszFileName;


            //System.IO.StreamWriter file1 = new System.IO.StreamWriter(strFilePath);
            //if (file1 == null)
                //return false;

            int i = 0;
            string test = string.Empty;

            using (System.IO.StreamWriter file = new System.IO.StreamWriter(strFilePath))
            {
                for (i = 0; i < m_iCount; i++)
                {
                    test = m_pElement[i].m_strVariableName + "," + "\"" + m_pElement[i].m_strValue + "\"" + "\n";
                    file.WriteLine(test);
                }
                
            }

            return true;
        }


    }
}
